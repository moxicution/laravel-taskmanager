<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Home route
Route::get('/home', 'TaskController@index');
Route::get('/task', 'TaskController@index');


// Task creation route
Route::get('/task/create', 'TaskController@create');

// Task post route
Route::post('/task/save', 'TaskController@save');

// Task delete route
Route::get('/task/delete/{id}', 'TaskController@delete');