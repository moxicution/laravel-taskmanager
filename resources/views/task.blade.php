@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $panel_heading }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action=" {{ $post_url }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Task name</label>
                                <div class="col-md-6">
                                    <input id="task_name" type="text" class="form-control" name="task_name" value="@if(isset($task)){{ $task->task_name }}@endif" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Status</label>
                                    <div class="col-md-6">

                                        <select class="form-control" id="task_status" name="task_status" required="">
                                            <?php
                                                if(isset($task) && !empty($task))
                                                {
                                                    switch($task->task_status)
                                                    {
                                                        case 0:
                                                            echo '<option value="0">Closed</option><option value="1">Open</option>';
                                                            break;
                                                        case 1:
                                                            echo '<option value="1">Open</option><option value="0">Closed</option>';
                                                            break;
                                                    }
                                                }
                                                else
                                                {
                                                    echo '<option value="1">Open</option><option value="0">Closed</option>';
                                                }
                                            ?>
                                        </select>

                                @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($button_text)
                                            {{ $button_text }}
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
