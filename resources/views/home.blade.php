@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tasks
                </div>
                <div class="panel-body">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                    <a href="{{ url('/task/create') }}"><button href="" type="button" class="btn btn-success pull-right"> Create new task</button></a>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Task</th>
                            <th>Status</th>
                            <th>Created on</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($tasks as $task)
                        <tr>
                            <td>
                                {{ $task->task_name }}
                            </td>

                            @if ($task->task_status == 1)
                                <td>
                                    <span class="label label-danger">Open</span>
                                </td>
                            @else
                                <td>
                                    <span class="label label-success">Closed</span>
                                </td>
                            @endif
                            <td>
                                {{ $task->task_created_on }}
                            </td>
                            <td>
                                <a href="{{ url('/task/delete', $task->task_id)  }}" data-placement="top" data-toggle="tooltip" title="Delete">
                                    <button class="btn btn-danger btn-xs" data-title="Delete" >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
    </div>
</div>
@endsection
