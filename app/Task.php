<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // Sets the values for this Eloquent ORM Model
    protected  $table = 'task';
    public $timestamps = false;
}
