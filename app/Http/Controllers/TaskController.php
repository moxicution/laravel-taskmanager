<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        // Uses auth middleware to check if the user is loggedin
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // Loads all tasks from database
        $tasks = Task::all();
        return view('home')->with('tasks', $tasks);

        unset($tasks);
    }

    /**
     * Create a new task viewloader.
     *
     * @return void
     */

    public function create()
    {
        // Loads 'task' View
        $data = array(
            'panel_heading' => 'Create Task',
            'button_text' => 'Create',
            'existing_task_info' => '',
            'post_url' => '/task/save/'
        );
        return view('task')->with($data);

        unset($data);
    }

    /**
     * Save the new task.
     *
     * @return void
     */

    public function save(Request $request)
    {
        // Validating user input
        $this->validate($request, [
            'task_name' => 'required',
            'task_status' => 'required'
        ]);

        // Instatiating new Task class
        $task = new Task;
        $task->task_name = $request->task_name;
        $task->task_status = $request->task_status;
        $task->task_created_on = date("Y-m-d h:i:s");

       if($task->save())
       {
           session()->flash('alert-success', 'Task successfully added!');
       }
       else
       {
           session()->flash('alert-danger', 'Task could not be created!');
       }

       return redirect('task');

       unset($task);
    }

    /**
     * Deletes a task.
     *
     * @return void
     */

    public function delete($task_id = null)
    {

        // force-casting $task_id into an (int) to prevent unwanted types of input
        $task_id = (int) $task_id;

        // Checking if 'task_id' isn't empty and the delete function is executed correctly
        if(!empty($task_id) && is_int($task_id) && Task::where('task_id', '=', $task_id)->delete())
        {
            session()->flash('alert-success', 'Task deleted successfully ');
        }
        else
        {
            session()->flash('alert-danger', 'Task not deleted');
        }

        return redirect('home');

        unset($task_id);
    }
}
