<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Deletes 'task' table
        DB::table('task')->delete();

        // Inserts data into 'task' table
        DB::table('task')->insert(array(

            array(
                'task_id' => 1,
                'task_name'=>'Contact carlos.schipper@gmail.com to confirm the recieval of this demo.',
                'task_status'=> 1,
                'task_created_on' => date("Y-m-d h:i:s"),
            ),
            array(
                'task_id' => 2,
                'task_name'=>'Offer Carlos Schipper the job. He\'s awesome',
                'task_status'=> 1,
                'task_created_on' => date("Y-m-d h:i:s"),
            ),
            array(
                'task_id' => 3,
                'task_name'=>'Start further correspondence with Carlos regarding the job offer.',
                'task_status'=> 1,
                'task_created_on' => date("Y-m-d h:i:s"),
            )
        ));
    }
}
