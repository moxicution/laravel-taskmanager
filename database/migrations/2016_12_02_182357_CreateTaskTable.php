<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create 'task' table and corresponding columns
        Schema::create('task', function (Blueprint $table) {
            $table->increments('task_id');
            $table->text('task_name');
            $table->integer('task_status');
            $table->timestamp('task_created_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Delete 'task' table
        Schema::dropIfExists('task');
    }
}
